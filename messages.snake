# cdshelf messages Snakefile
# Copyright (C) 2017, 2019  Marcel Schilling
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


#######################
# general information #
#######################

# file:        messages.snake
# created:     2017-03-26
# last update: 2019-02-17
# author:      Marcel Schilling <marcel.schilling@mdc-berlin.de>
# license:     GNU Affero General Public License Version 3 (GNU AGPL v3)
# purpose:     define message rules for Audio CD backup & conversion tool


######################################
# change log (reverse chronological) #
######################################

# 2019-02-17: formatted comments as sentences
#             replaced double quotes by single quotes
# 2017-03-26: initial version (help, usage & license)


###########
# imports #
###########

# Get message definitions.
import messages


############
# messages #
############

# Print help message.
rule help:
  output:
    temp(touch('help.done'))
  run:
    print(messages.help)

# Print usage message.
rule usage:
  run:
    print(messages.usage)

# Print license message.
rule license:
  run:
    print(messages.license)
